from base.models import Post, UserMessage
from django.forms import ModelForm

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'tags', 'categories', 'sender', 'content']
        
class UserMessageForm(ModelForm):
    class Meta:
        model = UserMessage
        fields = ['title', 'receiver', 'content', 'sender']