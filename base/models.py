from django.db import models
from taggit.managers import TaggableManager
from django.contrib.auth.models import User
from utils import get_icon_path
# user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

class BaseLink(models.Model):
    description = link = models.CharField(max_length=50)
    link = models.CharField(max_length=250)


class Category(models.Model):
    name = models.CharField(unique=True, max_length=100)
    tags = TaggableManager()
    icon = models.ImageField(upload_to=get_icon_path)
    
    def __unicode__(self):
        return u'%s'%(self.name)
    
    def icon_thumbnail(self):
        return '<img src="%s"/>' %self.icon.url 
    icon_thumbnail.allow_tags = True
    
class BaseMessage(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    sender = models.ForeignKey(User, related_name="sender_user")
    content = models.TextField()

class Post(BaseMessage):
    title = models.CharField(max_length=50)
    last_modified = models.DateTimeField(auto_now=True)
    tags = TaggableManager()
    categories = models.ManyToManyField(Category)

class UserMessage(BaseMessage):
    title = models.CharField(max_length=50)
    receiver = models.ForeignKey(User, related_name="reciver_user")

class PostLink(BaseLink):
    post = models.ForeignKey(Post)