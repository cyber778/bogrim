from django.contrib import admin
from base.models import  Category, PostLink, Post, UserMessage
# Example:
# class AccountAdmin(admin.ModelAdmin):
#     list_display = ["user", "partner_site"]
#     search_fields = ('user__email',)
#     raw_id_fields = ("user",)
# 
# class PresentoonAdmin(admin.ModelAdmin):
#     list_display = ('id', 'hashid', 'owner', 'name', 'created',
#         'last_modified', 'pending_backup', 'is_deleted')
#     list_display_links = ('name', 'id', 'hashid',)
#     list_filter = ('created', 'last_modified')
#     search_fields = ["name", "hashid", "id", "owner__username", "owner__email"]
#     raw_id_fields = ("owner",)
#     filter_horizontal = ('packages',)
#     change_form_template = 'presentoons/admin/presentoon_edit.html'

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('icon_thumbnail', 'name','id')
    list_filter = ('id', )

class BaseMessageAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'title', 'receiver', 'sender')
    raw_id_fields = ('receiver','sender')

class PostAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'title',  'owner')
    raw_id_fields = ('owner',)

# Register your models here.
admin.site.register(Category,CategoryAdmin)
admin.site.register(PostLink,)
admin.site.register(Post,)
admin.site.register(UserMessage, BaseMessageAdmin)
