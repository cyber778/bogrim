from django.core.management.base import BaseCommand
from base.models.models import Category
import random

class Command(BaseCommand):                   
    def handle(self, *args, **options):
        print "starting categories" 
        for n in range(0, 11):
            cat = "%s-קטגוריה"%n
            c = Category(name="project" + cat)
            print "created  category-> %s"%c
            
        print "finished categories"    
                    
        print "starting templates" 
        for n in range(1, 100):
            rand = random.randint(1,10)
            presentoon = Presentoon.objects.filter(is_deleted=False)[rand:rand+1]
            presentoon = presentoon[0]
            prand = random.randint(1,7)
            srand = random.randint(1,3)
            thumbrand = random.randint(1,3)
            name = "template%s"%n
            pcats = list(ProjectCategory.objects.all()[prand:prand+3])
            scats = list(StyleCategory.objects.all()[srand:srand+2])
            pt = ProjectTemplate(presentoon=presentoon, description="this is the best description ever", name="Project" + name, active=True, thumb = '/slides/thumb'+ str(thumbrand) + '.png', animated_thumb='/slides/animated_thumb'+ str(thumbrand) + '.gif' )
            st = StyleTemplate(presentoon=presentoon, description="this is the best description ever", name="Style" + name, active=True, thumb = '/slides/thumb'+ str(thumbrand) + '.png', animated_thumb='/slides/animated_thumb'+ str(thumbrand) + '.gif' )
            pt.save()
            st.save()
            pt.categories.add(*pcats)
            st.categories.add(*scats)
            
            print "created project template-> %s"%pt
            print "created style template-> %s"%st
                
            pt.save()
            st.save()
        print "finished templates" 
            
            