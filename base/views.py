from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from base.forms import PostForm, UserMessageForm
from base.models import Category, Post
from django.core.urlresolvers import reverse
import json

def home(request):
    category_list = Category.objects.all()
    post_list = Post.objects.all()
    ctx = {
        "categories":category_list,
        "posts":post_list
    }
    return render(request, 'home.html', ctx)

def create_post(request):
    if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('login') + "?next=create_post")
    if request.method == 'POST': # If the form has been submitted...
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            post_form.save()
            return HttpResponseRedirect(reverse('home'))
    else:
        post_form = PostForm()
    category_list = Category.objects.all()
    ctx = {
        "form":post_form,
        "categories":category_list
    }
    return render(request, 'base/post.html', ctx)

def send_message(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    message = {"status":"0", "error":""}
    if request.method == 'POST':
        message_form = UserMessageForm(request.POST)
        if message_form.is_valid():
            message_form.save()
            message['status'] = 1
        else:
            message['status'] = 1
            message['errors'] = form.errors
    else:
        message['status'] = 0
        message['errors'] = 'Hacking is bad for you!'
    simplejson = json.dumps(message)
    return HttpResponse(simplejson, content_type='application/json')