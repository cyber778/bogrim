from django.conf.urls import patterns, include, url
from base import views


urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^post/create/$', views.create_post,name="create_post"),
    url(r'^message/create/$', views.send_message,name="send_message"),
)