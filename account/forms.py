#import re
from django import forms
from django.contrib.auth import login
from django.utils.translation import ugettext_lazy as _, ugettext
from django.contrib.auth.models import User
from django.forms import ModelForm
from account.models import Student
from django.contrib.auth import authenticate

#alnum_re = re.compile(r"^\w+$")

class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ['image', 'tags', 'categories', 'user']



class LoginForm(forms.Form):
    email = forms.EmailField(
        label=_("Email*"),
        required=True,
        widget=forms.TextInput()
    )
    
    password = forms.CharField(
        label=_("Password*"),
        required=True,
        widget=forms.PasswordInput(render_value=False)
    )
    
    user = None
    
    def clean(self):
        if self._errors:
            return
        
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).count() == 0:
            raise forms.ValidationError("Wrong username or password")
        self.user = authenticate(username=self.cleaned_data['email'], password=self.cleaned_data['password'])
        if self.user is None:
            raise forms.ValidationError("Wrong username or password")
        elif not self.user.is_active:
            raise forms.ValidationError("This user has been deleted please contact support")
        return self.cleaned_data
    
    def self_login(self, request):
        login(request, self.user)
    
class SignupForm(forms.Form):

    first_name = forms.CharField(
        label=_("First Name"),
        widget=forms.TextInput()
    )

    last_name = forms.CharField(
        label=_("Last Name"),
        widget=forms.TextInput(),
        required=False
    )
    
    email = forms.EmailField(
        label=_("Email*"),
        required=True,
        widget=forms.TextInput()
    )

    password = forms.CharField(
        label=_("Password*"),
        required=True,
        widget=forms.PasswordInput(render_value=False)
    )

    def clean_email(self):
        value = self.cleaned_data["email"]
        if value:
            try:
                User.objects.get(email__iexact=value)
            except User.DoesNotExist:
                return value
            raise forms.ValidationError(_("A user is registered with this email address."))
        return value

    def create_user(self, commit=True):
        user = User()
        user.username = self.cleaned_data["email"].strip().lower()
        user.first_name = self.cleaned_data["first_name"].strip().lower()
        user.last_name = self.cleaned_data["last_name"].strip().lower()
        user.email = self.cleaned_data["email"].strip().lower()
        password = self.cleaned_data.get("password")
        user.set_password(password)
        if commit:
            user.save()
        return user
