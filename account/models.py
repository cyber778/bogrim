from django.db import models
from base.models import Category, BaseMessage, BaseLink
from taggit.managers import TaggableManager
from utils import get_image_path

from django.contrib.auth.models import User

class Reference(BaseMessage):
    pass

class MailMessage(BaseMessage):
    title = models.CharField(max_length=50)
    is_advertise = models.BooleanField(default=False)

class ProfileLinks(BaseLink):
    owner = models.ForeignKey(User)

class Student(models.Model):
    user = models.OneToOneField(User, unique=True)
    image = models.ImageField(null=True, blank=True, upload_to=get_image_path, default='/static/images/profile.jpg')
    tags = TaggableManager()
    categories = models.ManyToManyField(Category)

