from django.contrib import admin
from account.models import Reference, MailMessage, ProfileLinks, Student

class StudentAdmin(admin.ModelAdmin):
    list_display = ["id", "user"]
    search_fields = ('user__email','user__first_name', 'user__last_name')
    raw_id_fields = ("user",)
 

# Register your models here.
admin.site.register(Student,StudentAdmin)
admin.site.register(Reference,)
admin.site.register(MailMessage,)
admin.site.register(ProfileLinks,)