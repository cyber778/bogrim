from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.views import logout as auth_logout
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
from base.models import Category, Post, UserMessage
from account.models import Student
from account.forms import SignupForm, LoginForm, StudentForm
import json
from django.contrib.auth.decorators import login_required

# Create your views here.

#need to add ?next=....
def login(request):
    next_view = request.GET.get('next', 'home')
    if request.method == 'POST': # If the form has been submitted...
        form = LoginForm(request.POST)
        if form.is_valid():
            form.self_login(request)
            return HttpResponseRedirect((reverse(next_view)))
    else:
        form = LoginForm()
    ctx = {
        "form":form
    }
    return render(request, 'account/login.html',ctx)


def signup(request):
    if request.method == 'POST': # If the form has been submitted...
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.create_user()
            user = authenticate(username=user.username, password=form.cleaned_data['password'])
            auth_login(request, user)
            return render(request, 'account/thanks.html')
    else:
        form = SignupForm()
    ctx = {
        "categories":Category.objects.all(),
        "form":form,
    }
    return render(request, 'account/signup.html',ctx)



def create_student(request):
    if request.method == 'POST': # If the form has been submitted...
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('home'))
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'account/thanks.html')
        ctx = {
            "categories":Category.objects.all(),
            "form":form,
        }
        return render(request, 'account/signup.html',ctx)
    else:
        return HttpResponse("POST only")

def test(request):
    return render(request, 'account/thanks.html')

def logout(request, next_page=None, **kwargs):
    # Simple Wrapper around django.contrib.auth.views.logout to default
    #    next_page based off the setting LOGOUT_REDIRECT_URLNAME.
    next_page = request.GET.get('next_page', None)
    
    if next_page is None and hasattr(settings, "LOGOUT_REDIRECT_URLNAME"):
        next_page = reverse(settings.LOGOUT_REDIRECT_URLNAME)

    return auth_logout(request, next_page, **kwargs)


def account_info(request, user_id=False):
    viewed_user = get_object_or_404(User, id=user_id) if user_id else request.user
    editable = True if viewed_user==request.user else False
    user_posts = Post.objects.filter(sender=viewed_user)
    user_messages = UserMessage.objects.filter(receiver=viewed_user)
    student = get_object_or_404(Student, user=viewed_user)
    messages = UserMessage.objects.filter(receiver=request.user)
    ctx={
         "messages":messages,
         "editable":editable,
         "user_posts":user_posts,
         "viewed_user":viewed_user,
         "student":viewed_user.student,
         "categories":Category.objects.all(),
         "user_posts":user_posts,
         }
    return render(request, 'account/info.html',ctx)

@login_required
def update_profile(request):
    message = {"status":"0", "error":""}
    if request.is_ajax():
        user = request.user
        student = request.user.student
        field_name = request.POST.get('field1_name', False)
        field_value = request.POST.get('field1_val', False)
        if field_name is False or field_value is False:
            message['status'] = 0
            message['error'] = 'empty form values'
        elif request.POST['model'] == "user":
            setattr(user, field_name, field_value)
            if request.POST['field2_name']:
                setattr(user, request.POST['field2_name'], request.POST['field2_val'])
                message['field_name'] = 'name'
                message['field_value'] = field_value + ' ' + request.POST['field2_val']
            else:
                message['field_name'] = field_name
                message['field_value'] = field_value
                message['field2_name'] = False
            user.save();
        else:
            setattr(student, field_name, field_value)
            student.save()
        message['status'] = 1
    else:
        message['status'] = 0
        message['error'] = 'Hacking is bad for you!'
    simplejson = json.dumps(message)
    return HttpResponse(simplejson, content_type='application/json')

def a404(request):
     return render(request, '404.html')