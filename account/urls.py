from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # existing patterns here...
    url(r'^test/$',  'account.views.test', name="test"),    #add template ctx
    url(r'^signup/$', 'account.views.signup', name="signup"),
    url(r'^login/$', 'account.views.login', name="login"),
    url(r'^logout/$', 'account.views.logout', name="logout"),
    url(r'^create/student/$', 'account.views.create_student', name="create_student"),
    url(r'^info/$', 'account.views.account_info', name="account_info"),
    url(r'^update/$', 'account.views.update_profile', name="update_profile"),
    url(r'^info/(\d+)/$', 'account.views.account_info', name="update_profile"),
    url(r'^404/$', 'account.views.a404', name="404"),
)

