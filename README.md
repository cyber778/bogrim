All Client side done by tamir, Server side by Elad

as of October Elad is fixing the client side step by step:

- moving css to sass

- making the design responsive

- removing all relative and absolute css positioning

- removing images

- removing templates like *_script.html

- better UI 